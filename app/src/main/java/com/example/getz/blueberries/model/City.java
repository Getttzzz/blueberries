package com.example.getz.blueberries.model;

public class City {

    private String mCityId;
    private String mCityName;
    private String mCityCode;

    private String mCurrentTemp;
    private String mMinTemp;
    private String mMaxTemp;
    private String mPressure;
    private String mHumidity;

    private String mSpeed;
    private String mDeg;

    public City() {

    }

    public String getCurrentTemp() {
        return mCurrentTemp;
    }

    public void setCurrentTemp(String pCurrentTemp) {
        mCurrentTemp = pCurrentTemp;
    }

    public String getMinTemp() {
        return mMinTemp;
    }

    public void setMinTemp(String pMinTemp) {
        mMinTemp = pMinTemp;
    }

    public String getMaxTemp() {
        return mMaxTemp;
    }

    public void setMaxTemp(String pMaxTemp) {
        mMaxTemp = pMaxTemp;
    }

    public String getPressure() {
        return mPressure;
    }

    public void setPressure(String pPressure) {
        mPressure = pPressure;
    }

    public String getHumidity() {
        return mHumidity;
    }

    public void setHumidity(String pHumidity) {
        mHumidity = pHumidity;
    }

    public String getCityId() {
        return mCityId;
    }

    public void setCityId(String pCityId) {
        mCityId = pCityId;
    }

    public String getCityName() {
        return mCityName;
    }

    public void setCityName(String pCityName) {
        mCityName = pCityName;
    }

    public String getCityCode() {
        return mCityCode;
    }

    public void setCityCode(String pCityCode) {
        mCityCode = pCityCode;
    }

    public String getSpeed() {
        return mSpeed;
    }

    public void setSpeed(String pSpeed) {
        mSpeed = pSpeed;
    }

    public String getDeg() {
        return mDeg;
    }

    public void setDeg(String pDeg) {
        mDeg = pDeg;
    }
}
