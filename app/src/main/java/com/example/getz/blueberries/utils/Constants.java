package com.example.getz.blueberries.utils;


public class Constants {

    public static final String BASE_URL = "http://api.openweathermap.org";
    public static final String API_KEY = "&appid=749fb052efd255a9d8eb22be3de1bb83";

    public static final String TAG_CITY_NAME = "name";
    public static final String TAG_CITY_ID = "id";
    public static final String TAG_CITY_CODE = "cod";

    public static final String TAG_MAIN = "main";
    public static final String TAG_MAIN_TEMP = "temp";
    public static final String TAG_MAIN_TEMP_MIN = "temp_min";
    public static final String TAG_MAIN_TEMP_MAX = "temp_max";
    public static final String TAG_MAIN_HUMIDITY = "humidity";
    public static final String TAG_MAIN_PRESSURE = "pressure";

    public static final String TAG_WIND = "wind";
    public static final String TAG_WIND_SPEED = "speed";
    public static final String TAG_WIND_DEG = "deg";

}

