package com.example.getz.blueberries.net;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.example.getz.blueberries.model.City;
import com.example.getz.blueberries.utils.Constants;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Getz on 30.12.2015.
 */
public class NetWorker extends AsyncTask<Void, Void, Void> {

    private String mJsonString;
    private String mRequestCityName;
    private ProgressDialog mProgressDialog;
    private OkHttpClient mOkHttpClient;

    private JSONObject mMainJSONObject;
    private JSONObject mWeatherJSONObject;
    private JSONObject mWindJSONObject;

    private TextView tvCityName;
    private TextView tvCityCode;
    private TextView tvCityId;
    private TextView tvCityCurrentTemp;
    private TextView tvCityMinTemp;
    private TextView tvCityMaxTemp;
    private TextView tvCityHumidity;
    private TextView tvCityPressure;
    private TextView tvWindSpeed;
    private TextView tvWindDeg;

    private City mCity;


    public NetWorker(OkHttpClient pOkHttpClient, String pRequestCityName, ProgressDialog pProgressDialog) {
        mOkHttpClient = pOkHttpClient;
        mProgressDialog = pProgressDialog;
        mRequestCityName = pRequestCityName;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            mJsonString = doGetRequest(Constants.BASE_URL + "/data/2.5/weather?q=" + mRequestCityName + Constants.API_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mMainJSONObject = new JSONObject(mJsonString);

            mCity = new City();

            mCity.setCityName(mMainJSONObject.getString(Constants.TAG_CITY_NAME));
            mCity.setCityCode(mMainJSONObject.getString(Constants.TAG_CITY_CODE));
            mCity.setCityId(mMainJSONObject.getString(Constants.TAG_CITY_ID));

            mWeatherJSONObject = mMainJSONObject.getJSONObject(Constants.TAG_MAIN);
            mCity.setCurrentTemp(mWeatherJSONObject.getString(Constants.TAG_MAIN_TEMP));
            mCity.setMinTemp(mWeatherJSONObject.getString(Constants.TAG_MAIN_TEMP_MIN));
            mCity.setMaxTemp(mWeatherJSONObject.getString(Constants.TAG_MAIN_TEMP_MAX));
            mCity.setHumidity(mWeatherJSONObject.getString(Constants.TAG_MAIN_HUMIDITY));
            mCity.setPressure(mWeatherJSONObject.getString(Constants.TAG_MAIN_PRESSURE));

            mWindJSONObject = mMainJSONObject.getJSONObject(Constants.TAG_WIND);
            mCity.setSpeed(mWindJSONObject.getString(Constants.TAG_WIND_SPEED));
            mCity.setDeg(mWindJSONObject.getString(Constants.TAG_WIND_DEG));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void pString) {
        super.onPostExecute(pString);
        mProgressDialog.dismiss();

        tvCityId.setText(mCity.getCityId());
        tvCityName.setText(mCity.getCityName());
        tvCityCode.setText(mCity.getCityCode());
        tvCityMaxTemp.setText(mCity.getMaxTemp());
        tvCityMinTemp.setText(mCity.getMinTemp());
        tvCityPressure.setText(mCity.getPressure());
        tvCityHumidity.setText(mCity.getHumidity());
        tvCityCurrentTemp.setText(mCity.getCurrentTemp());
        tvWindSpeed.setText(mCity.getSpeed());
        tvWindDeg.setText(mCity.getDeg());

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog.show();
    }

    private String doGetRequest(String pUrl) throws IOException {
        Request request = new Request.Builder().url(pUrl).build();
        Response response = mOkHttpClient.newCall(request).execute();
        return response.body().string();
    }

    public void setTvCityId(TextView pTvCityId) {
        tvCityId = pTvCityId;
    }

    public void setTvCityCode(TextView pTvCityCode) {
        tvCityCode = pTvCityCode;
    }

    public void setTvCityName(TextView pTvCityName) {
        tvCityName = pTvCityName;
    }

    public void setTvCityPressure(TextView pTvCityPressure) {
        tvCityPressure = pTvCityPressure;
    }

    public void setTvCityHumidity(TextView pTvCityHumidity) {
        tvCityHumidity = pTvCityHumidity;
    }

    public void setTvCityMaxTemp(TextView pTvCityMaxTemp) {
        tvCityMaxTemp = pTvCityMaxTemp;
    }

    public void setTvCityMinTemp(TextView pTvCityMinTemp) {
        tvCityMinTemp = pTvCityMinTemp;
    }

    public void setTvCityCurrentTemp(TextView pTvCityCurrentTemp) {
        tvCityCurrentTemp = pTvCityCurrentTemp;
    }

    public void setTvWindDeg(TextView pTvWindDeg) {
        tvWindDeg = pTvWindDeg;
    }

    public void setTvWindSpeed(TextView pTvWindSpeed) {
        tvWindSpeed = pTvWindSpeed;
    }
}
