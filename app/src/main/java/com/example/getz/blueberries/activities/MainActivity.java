package com.example.getz.blueberries.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.getz.blueberries.R;
import com.example.getz.blueberries.net.NetWorker;
import com.squareup.okhttp.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    private TextView tvCityId;
    private TextView tvCityName;
    private TextView tvCityCode;
    private TextView tvCityMinTemp;
    private TextView tvCityMaxTemp;
    private TextView tvCityHumidity;
    private TextView tvCityPressure;
    private TextView tvCityCurrentTemp;
    private TextView tvWindSpeed;
    private TextView tvWindDeg;

    private EditText etCity;
    private Button btWeather;
    private String mRequestCityName;
    private OkHttpClient mOkHttpClient;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initProgressDialog();
        initToolbar();
        initView();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        btWeather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mRequestCityName = etCity.getText().toString().trim();
                mOkHttpClient = new OkHttpClient();

                NetWorker netWorker = new NetWorker(mOkHttpClient, mRequestCityName, mProgressDialog);
                netWorker.setTvCityName(tvCityName);
                netWorker.setTvCityId(tvCityId);
                netWorker.setTvCityCode(tvCityCode);
                netWorker.setTvCityCurrentTemp(tvCityCurrentTemp);
                netWorker.setTvCityHumidity(tvCityHumidity);
                netWorker.setTvCityMaxTemp(tvCityMaxTemp);
                netWorker.setTvCityMinTemp(tvCityMinTemp);
                netWorker.setTvCityPressure(tvCityPressure);
                netWorker.setTvWindSpeed(tvWindSpeed);
                netWorker.setTvWindDeg(tvWindDeg);
                netWorker.execute();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("loading...");
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initView() {
        tvCityName = (TextView) findViewById(R.id.am_city_name);
        tvCityId = (TextView) findViewById(R.id.am_city_id);
        tvCityCode = (TextView) findViewById(R.id.am_city_code);
        etCity = (EditText) findViewById(R.id.am_et_city);
        btWeather = (Button) findViewById(R.id.am_bt_weather);
        tvCityCurrentTemp = (TextView) findViewById(R.id.am_city_current_temp);
        tvCityMinTemp = (TextView) findViewById(R.id.am_city_min_temp);
        tvCityMaxTemp = (TextView) findViewById(R.id.am_city_max_temp);
        tvCityHumidity = (TextView) findViewById(R.id.am_city_humidity);
        tvCityPressure = (TextView) findViewById(R.id.am_city_pressure);
        tvWindSpeed = (TextView) findViewById(R.id.am_wind_speed);
        tvWindDeg = (TextView) findViewById(R.id.am_wind_deg);
    }


}
